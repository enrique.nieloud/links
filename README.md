# Links

## Test and Deploy

### DeepLinks
- [Preguntados](preguntados://triviatop/blah)

### SwiftyMocky
* [SwiftyMocky](https://github.com/MakeAWishFoundation/SwiftyMocky#mock-annotate)
* [Command Line Interface](https://github.com/MakeAWishFoundation/SwiftyMocky/blob/master/guides/Command%20Line%20Interface.md)
* [Using SwiftyMocky to generate mocks and simplify unit testing in Swift](http://blog.girappe.com/?swiftymocky)

### Etermax
* [Beat The Master - Documentos de Google](https://docs.google.com/document/d/19z32qCC7x3Q-JrqQFc9EtJGUHyE4v4ONlKwZY72EOMQ/edit#heading=h.7cvhxn9cvvi2)
* [Flujo 1](https://xd.adobe.com/view/37bcde23-e96b-476b-943b-06006a0bf566-62ef/)
* [Enrique Nieloud / TwitterKata · GitLab](https://gitlab.com/enrique.nieloud/twitterkata)
* [Home · Wiki · Etermax / Learning / Onboarding / onboarding-process · GitLab](https://gitlab.com/etermax/learning/onboarding/onboarding-process/-/wikis/home)
* [RxSwift: Reactive Programming with Swift, Chapter 2: Observables | raywenderlich.com](https://www.raywenderlich.com/books/rxswift-reactive-programming-with-swift/v4.0/chapters/2-observables#toc-chapter-006-anchor-008)
* [Twitter Kata · Wiki · Etermax / Learning / Onboarding / onboarding-process · GitLab](https://gitlab.com/etermax/learning/onboarding/onboarding-process/-/wikis/Twitter-Kata)
* [etercoding 30/07 - Kata RPG con Interaction Driven Design (IDD) en Kotlin - YouTube](https://www.youtube.com/watch?v=rPeLeIpG0g0&t=1s)
* [etercoding 30/07 - Kata RPG con Interaction Driven Design (IDD) en Kotlin - YouTube](https://www.youtube.com/watch?v=rPeLeIpG0g0)
* [Slite](https://etermax.slite.com/app/channels/tCS9KsH3Tz/notes/G8RHmrq1dO)
* [Atlas - Google Drive](https://drive.google.com/drive/folders/1ARhVd_cfsNrVzroqoBSjGwRgZRF9GUvv)
* [Slite](https://etermax.slite.com/app/filters/catchup/notes)

### RxSwift
* [RxSwift/GettingStarted.md at main · ReactiveX/RxSwift · GitHub](https://github.com/ReactiveX/RxSwift/blob/main/Documentation/GettingStarted.md)
* [swift - How to import RxSwift in Playground of xcode? - Stack Overflow](https://stackoverflow.com/questions/42082864/how-to-import-rxswift-in-playground-of-xcode)
* [Making a playground using RxSwift | by Andy Chou | Medium](https://medium.com/@_achou/making-a-playground-using-rxswift-81d8377bd239)
* [rxs-materials - githubmemory](https://githubmemory.com/repo/raywenderlich/rxs-materials/activity?page=2)
* [raywenderlich/rxs-materials - githubmemory](https://githubmemory.com/repo/raywenderlich/rxs-materials)
* [GitHub - raywenderlich/rxs-materials at editions/5.0](https://github.com/raywenderlich/rxs-materials/tree/editions/5.0)
* [Learn & Master ⚔️ the Basics of RxSwift in 10 Minutes | by Sebastian Boldt | iOS App Development | Medium](https://medium.com/ios-os-x-development/learn-and-master-%EF%B8%8F-the-basics-of-rxswift-in-10-minutes-818ea6e0a05b)
* [Combine vs RxSwift: Introducción a Combine y diferencias](https://apiumhub.com/es/tech-blog-barcelona/combine-vs-rxswift-diferencias/)
* [MVVM + Coordinators + RxSwift and sample iOS application with authentication – Wojciech Kulik](https://wojciechkulik.pl/ios/mvvm-coordinators-rxswift-and-sample-ios-application-with-authentication)
* [RxSwift Traits. Dive into Single, Completable and Maybe | by Aaina jain | Swift India | Medium](https://medium.com/swift-india/rxswift-traits-5240965c4f12)
* [rxswift - YouTube](https://www.youtube.com/results?search_query=rxswift)
* [MVVM + RxSwift on iOS part 1 | Hacker Noon](https://hackernoon.com/mvvm-rxswift-on-ios-part-1-69608b7ed5cd)
* [Reactive programming in Swift with RxSwift and RxCocoa [Tutorial] | Packt Hub](https://hub.packtpub.com/reactive-programming-in-swift-with-rxswift-and-rxcocoa-tutorial/)
* [MVVM & rxSwift](https://medium.com/flawless-app-stories/practical-mvvm-rxswift-a330db6aa693)
* [MVVM & rxSwift](https://www.vincit.com/blog/how-to-use-rxswift-with-mvvm-pattern)

### Combine
* [Intro](https://www.swiftbysundell.com/basics/combine/)
* [Intro](https://www.avanderlee.com/swift/combine/)
* [Intro](https://medium.com/ios-os-x-development/learn-master-%EF%B8%8F-the-basics-of-combine-in-5-minutes-639421268219)
* [Intro en español](https://www.adictosaltrabajo.com/2020/05/18/primeros-pasos-con-combine/)
* [Online Book](https://heckj.github.io/swiftui-notes/#aboutthisbook)
* [Curso](https://cocoacasts.com/building-reactive-applications-with-combine-what-is-reactive-programming)
* [The ultimate Combine framework tutorial in Swift](https://theswiftdev.com/the-ultimate-combine-framework-tutorial-in-swift/)
* [Combine Demo MVVM](https://github.com/mcichecki/combine-mvvm)
* [UITableView and Combine](https://github.com/CombineCommunity/CombineDataSources#swift-package-manager)
* [Property Wrappers in Swift explained with code examples](https://www.avanderlee.com/swift/property-wrappers/)
* [Bindable values in swift](https://www.swiftbysundell.com/articles/bindable-values-in-swift/)
* [Componentes UIKit](https://github.com/CombineCommunity/CombineCocoa)

### IDD y S.O.L.I.D
* [Interaction Driven Design | Codurance](https://www.codurance.com/publications/videos/2015-04-24-interaction-driven-design)
* [London Vs. Chicago (Comparative Case Study) - Google Drive](https://drive.google.com/drive/u/0/folders/1yo-lSK4BeOw7sVtwl9YKpZHylEFqxsms)
* [Introducing Interaction-Driven Design | Codurance](https://www.codurance.com/publications/2017/12/08/introducing-idd)
* [Capacitacion IDD - Presentaciones de Google](https://docs.google.com/presentation/d/1-py4f41xl9aBvUcs5S7kGcwFbnbzwv6YGf9KThXJUOo/edit#slide=id.g86476d8cc3_1_479)
* [Better iOS Apps with Interaction-Driven Design | by Pablo Manuelli | Level Up Coding](https://levelup.gitconnected.com/better-ios-apps-with-interaction-driven-design-f9187e745641)
* [GitHub - pmanuelli/social-networking: Social Networking App Example](https://github.com/pmanuelli/social-networking)
* [Tutorials | Vaadin](https://vaadin.com/learn/tutorials/ddd/tactical_domain_driven_design)
* [Uncle Bob SOLID principles - YouTube](https://www.youtube.com/watch?v=zHiWqnTWsn4)
* [Clean Coder Blog](https://blog.cleancoder.com/uncle-bob/2013/05/27/TheTransformationPriorityPremise.html)

### Testing
* [How do you call XCTest in a Swift Playground? - Stack Overflow](https://stackoverflow.com/questions/25051159/how-do-you-call-xctest-in-a-swift-playground/43689317)
* [“How do you think when writing tests?” – It’s simpler than you may think - DEV Community](https://dev.to/essentialdeveloper/how-do-you-think-when-writing-tests--its-simpler-than-you-may-think-4ojk)
* [How to test software: mocking, stubbing, and contract testing](https://circleci.com/blog/how-to-test-software-part-i-mocking-stubbing-and-contract-testing/)

### VAPOR Rest API
* [Crea tu API Rest con Vapor, y una Arquitectura de microservicios | by Andres Felipe Ocampo | Medium](https://andresfelipeocampo.medium.com/crea-tu-api-rest-con-vapor-y-una-arquitectura-de-microservicios-7ff54c807a03)
* [REST API with Swift on Vapor. Server-side Swift introduction in 5… | by Valerii Che | Medium](https://medium.com/@myltik/rest-api-with-swift-on-vapor-fca5ea6667f)
* [Getting Started with Server-Side Swift with Vapor 4 | raywenderlich.com](https://www.raywenderlich.com/11555468-getting-started-with-server-side-swift-with-vapor-4)
* [RESTful APIs Tutorial: How to Create Your Own REST Library in Swift](https://www.appcoda.com/restful-api-library-swift/)

### Xcode and other
* [Finding and Refactoring Code | Apple Developer Documentation](https://developer.apple.com/documentation/xcode/finding-and-refactoring-code)
* [Sub-modules for Xcode. It is an easy task to create… | by Ralph Bergmann | Karlmax Berlin | Medium](https://medium.com/karlmax-berlin/sub-modules-for-xcode-acb6b1e5f567)
* [GitHub - the4thfloor/xcode-submodules](https://github.com/the4thfloor/xcode-submodules)
* [CocoaPods Guides - Specs and the Specs Repo](https://guides.cocoapods.org/making/specs-and-specs-repo.html)


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:b73055ac8e26fbbae09dcb49aab75c7e?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:b73055ac8e26fbbae09dcb49aab75c7e?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:b73055ac8e26fbbae09dcb49aab75c7e?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/enrique.nieloud/links.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:b73055ac8e26fbbae09dcb49aab75c7e?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:b73055ac8e26fbbae09dcb49aab75c7e?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:b73055ac8e26fbbae09dcb49aab75c7e?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:b73055ac8e26fbbae09dcb49aab75c7e?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:b73055ac8e26fbbae09dcb49aab75c7e?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:b73055ac8e26fbbae09dcb49aab75c7e?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:b73055ac8e26fbbae09dcb49aab75c7e?https://docs.gitlab.com/ee/user/application_security/sast/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:b73055ac8e26fbbae09dcb49aab75c7e?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

